USE [master]
GO
/****** Object:  Database [LearningPortal]    Script Date: 9/13/2021 3:11:09 PM ******/
CREATE DATABASE [LearningPortal]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'LearningPortal', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLSERVER\MSSQL\DATA\LearningPortal.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'LearningPortal_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLSERVER\MSSQL\DATA\LearningPortal_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [LearningPortal] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [LearningPortal].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [LearningPortal] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [LearningPortal] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [LearningPortal] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [LearningPortal] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [LearningPortal] SET ARITHABORT OFF 
GO
ALTER DATABASE [LearningPortal] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [LearningPortal] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [LearningPortal] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [LearningPortal] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [LearningPortal] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [LearningPortal] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [LearningPortal] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [LearningPortal] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [LearningPortal] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [LearningPortal] SET  DISABLE_BROKER 
GO
ALTER DATABASE [LearningPortal] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [LearningPortal] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [LearningPortal] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [LearningPortal] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [LearningPortal] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [LearningPortal] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [LearningPortal] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [LearningPortal] SET RECOVERY FULL 
GO
ALTER DATABASE [LearningPortal] SET  MULTI_USER 
GO
ALTER DATABASE [LearningPortal] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [LearningPortal] SET DB_CHAINING OFF 
GO
ALTER DATABASE [LearningPortal] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [LearningPortal] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [LearningPortal] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [LearningPortal] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
EXEC sys.sp_db_vardecimal_storage_format N'LearningPortal', N'ON'
GO
ALTER DATABASE [LearningPortal] SET QUERY_STORE = OFF
GO
USE [LearningPortal]
GO
/****** Object:  Table [dbo].[Addres]    Script Date: 9/13/2021 3:11:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Addres](
	[AddresID] [int] IDENTITY(1,1) NOT NULL,
	[Addres] [nvarchar](100) NOT NULL,
	[SubCourseID] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[AddresID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Course]    Script Date: 9/13/2021 3:11:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Course](
	[CourseID] [int] IDENTITY(1,1) NOT NULL,
	[CourseName] [nvarchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[CourseID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SubCourse]    Script Date: 9/13/2021 3:11:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SubCourse](
	[SubCourseID] [int] IDENTITY(1,1) NOT NULL,
	[SubCourseName] [nvarchar](100) NOT NULL,
	[MainCourseID] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[SubCourseID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Addres] ON 

INSERT [dbo].[Addres] ([AddresID], [Addres], [SubCourseID]) VALUES (1, N'https://www.youtube.com/embed/TMubSggUOVE', 2)
INSERT [dbo].[Addres] ([AddresID], [Addres], [SubCourseID]) VALUES (2, N'https://www.youtube.com/embed/bEMIicZhUCM', 3)
INSERT [dbo].[Addres] ([AddresID], [Addres], [SubCourseID]) VALUES (3, N'https://www.youtube.com/embed/zbMHLJ0dY4w', 4)
INSERT [dbo].[Addres] ([AddresID], [Addres], [SubCourseID]) VALUES (4, N'https://www.youtube.com/embed/27axs9dO7AE', 5)
INSERT [dbo].[Addres] ([AddresID], [Addres], [SubCourseID]) VALUES (6, N'https://www.youtube.com/embed/rfscVS0vtbw', 8)
INSERT [dbo].[Addres] ([AddresID], [Addres], [SubCourseID]) VALUES (7, N'https://www.youtube.com/embed/xWsJ_qk-eJE', 9)
INSERT [dbo].[Addres] ([AddresID], [Addres], [SubCourseID]) VALUES (13, N'https://www.youtube.com/embed/NybHckSEQBI', 2)
INSERT [dbo].[Addres] ([AddresID], [Addres], [SubCourseID]) VALUES (14, N'https://www.youtube.com/embed/eIrMbAQSU34', 12)
INSERT [dbo].[Addres] ([AddresID], [Addres], [SubCourseID]) VALUES (15, N'https://www.youtube.com/embed/eIrMbAQSU34', 12)
INSERT [dbo].[Addres] ([AddresID], [Addres], [SubCourseID]) VALUES (16, N'https://www.youtube.com/embed/gfkTfcpWqAY', 14)
INSERT [dbo].[Addres] ([AddresID], [Addres], [SubCourseID]) VALUES (17, N'https://www.youtube.com/embed/E7Voso411Vs', 15)
SET IDENTITY_INSERT [dbo].[Addres] OFF
GO
SET IDENTITY_INSERT [dbo].[Course] ON 

INSERT [dbo].[Course] ([CourseID], [CourseName]) VALUES (1, N'math')
INSERT [dbo].[Course] ([CourseID], [CourseName]) VALUES (3, N'sql')
INSERT [dbo].[Course] ([CourseID], [CourseName]) VALUES (4, N'Python')
INSERT [dbo].[Course] ([CourseID], [CourseName]) VALUES (5, N'JAVA')
INSERT [dbo].[Course] ([CourseID], [CourseName]) VALUES (6, N'c#')
INSERT [dbo].[Course] ([CourseID], [CourseName]) VALUES (7, N'ASP.NET')
SET IDENTITY_INSERT [dbo].[Course] OFF
GO
SET IDENTITY_INSERT [dbo].[SubCourse] ON 

INSERT [dbo].[SubCourse] ([SubCourseID], [SubCourseName], [MainCourseID]) VALUES (2, N'Introduction to Maths', 1)
INSERT [dbo].[SubCourse] ([SubCourseID], [SubCourseName], [MainCourseID]) VALUES (3, N'Advance maths', 1)
INSERT [dbo].[SubCourse] ([SubCourseID], [SubCourseName], [MainCourseID]) VALUES (4, N'Introduction to Sql', 3)
INSERT [dbo].[SubCourse] ([SubCourseID], [SubCourseName], [MainCourseID]) VALUES (5, N'Just new sql ', 3)
INSERT [dbo].[SubCourse] ([SubCourseID], [SubCourseName], [MainCourseID]) VALUES (6, N'advance', 3)
INSERT [dbo].[SubCourse] ([SubCourseID], [SubCourseName], [MainCourseID]) VALUES (8, N'Beginners to python', 4)
INSERT [dbo].[SubCourse] ([SubCourseID], [SubCourseName], [MainCourseID]) VALUES (9, N'Pro Pyhton', 4)
INSERT [dbo].[SubCourse] ([SubCourseID], [SubCourseName], [MainCourseID]) VALUES (10, N'MATHs', 1)
INSERT [dbo].[SubCourse] ([SubCourseID], [SubCourseName], [MainCourseID]) VALUES (12, N'Beginners to Java', 5)
INSERT [dbo].[SubCourse] ([SubCourseID], [SubCourseName], [MainCourseID]) VALUES (13, N'Java', 5)
INSERT [dbo].[SubCourse] ([SubCourseID], [SubCourseName], [MainCourseID]) VALUES (14, N'C# Junior', 6)
INSERT [dbo].[SubCourse] ([SubCourseID], [SubCourseName], [MainCourseID]) VALUES (15, N'Introduction to Asp.Net', 7)
SET IDENTITY_INSERT [dbo].[SubCourse] OFF
GO
ALTER TABLE [dbo].[Addres]  WITH CHECK ADD FOREIGN KEY([SubCourseID])
REFERENCES [dbo].[SubCourse] ([SubCourseID])
GO
ALTER TABLE [dbo].[SubCourse]  WITH CHECK ADD FOREIGN KEY([MainCourseID])
REFERENCES [dbo].[Course] ([CourseID])
GO
USE [master]
GO
ALTER DATABASE [LearningPortal] SET  READ_WRITE 
GO

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LearningPortal.Models;

namespace LearningPortal.Controllers
{
    public class AddresController : Controller
    {
        private LearningPortalEntities db = new LearningPortalEntities();

        
         static int idd;
        
        public ActionResult Index(int id)
        { 
            idd = id;
            var Add = db.Addres.Include(s => s.SubCourse).Where(q => q.SubCourseID.Equals(idd));
            return View(Add.ToList());
        }

        
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LearningPortal.Models;

namespace LearningPortal.Controllers
{
    public class CoursesController : Controller
    {
        private LearningPortalEntities db = new LearningPortalEntities();

        // GET: Courses
        public ActionResult Index()
        {
            return View(db.Courses.ToList());
        }
        [HttpPost]
    
        public ActionResult Index(int? id)
        {
            var val = id;
            return View();

        }
        public ActionResult Details(int? id)
        {
           
            return RedirectToAction("Index", "SubCourses", new { id });
        }


    }
}

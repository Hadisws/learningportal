﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LearningPortal.Models;

namespace LearningPortal.Controllers
{
    public class SubCoursesController : Controller
    {
        private LearningPortalEntities db = new LearningPortalEntities();

        
        public ActionResult Index(int id)
        {
            int idd = id;

            var subCourses = db.SubCourses.Include(s => s.Course).Where(q=>q.MainCourseID.Equals(idd));
            return View(subCourses.ToList());
        }

       
        public ActionResult Details(int? id)
        {

            return RedirectToAction("Index", "Addres", new { id });
        }

      
        

        
    }
}
